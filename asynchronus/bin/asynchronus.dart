import 'package:asynchronus/asynchronus.dart' as asynchronus;
import 'dart:async';

void main(List<String> args) async {
  print('Let sing Love Me Harder - Ariana Grande');
  print('1..2..3..');
  print(await lyrics());
  print(await lyrics1());
  print(await lyrics2());
  print(await lyrics3());
  print(await lyrics4());
  print(await lyrics5());
  print(await lyrics6());
  print(await lyrics7());
  print(await lyrics8());
  print(await lyrics9());
  print(await lyrics10());
  print(await lyrics11());
  print(await lyrics12());
}

Future<String> lyrics() async {
  String lyric = 'Tell me something I need to know';
  return await Future.delayed(Duration(seconds: 1), () => (lyric));
}

Future<String> lyrics1() async {
  String lyric = 'Then take my breath and never let it go';
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}

Future<String> lyrics2() async {
  String lyric = 'If you just let me invade your space';
  return await Future.delayed(Duration(seconds: 4), () => (lyric));
}

Future<String> lyrics3() async {
  String lyric = 'Ill take the pleasure, take it with the pain';
  return await Future.delayed(Duration(seconds: 6), () => (lyric));
}

Future<String> lyrics4() async {
  String lyric = 'And if in the moment I bite my lip';
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}

Future<String> lyrics5() async {
  String lyric = 'Baby, in that moment youll know this is';
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}

Future<String> lyrics6() async {
  String lyric = 'Something bigger than us and beyond bliss';
  return await Future.delayed(Duration(seconds: 4), () => (lyric));
}

Future<String> lyrics7() async {
  String lyric = 'Give me a reason to believe it';
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}

Future<String> lyrics8() async {
  String lyric =
      'Cause if you want to keep me, you gotta, gotta, gotta, gotta, got to love me harder';
  return await Future.delayed(Duration(seconds: 4), () => (lyric));
}

Future<String> lyrics9() async {
  String lyric =
      'And if you really need me, you gotta, gotta, gotta, gotta, got to love me harder';
  return await Future.delayed(Duration(seconds: 10), () => (lyric));
}

Future<String> lyrics10() async {
  String lyric = 'Baby, love me harder';
  return await Future.delayed(Duration(seconds: 7), () => (lyric));
}

Future<String> lyrics11() async {
  String lyric = 'Ooh, ooh, ooh, ooh Love me, love me, love me';
  return await Future.delayed(Duration(seconds: 3), () => (lyric));
}

Future<String> lyrics12() async {
  String lyric = 'Ooh, ooh, ooh, ooh Harder, harder, harder';
  return await Future.delayed(Duration(seconds: 5), () => (lyric));
}
