import 'package:dart_basic_programming/dart_basic_programming.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });
}
