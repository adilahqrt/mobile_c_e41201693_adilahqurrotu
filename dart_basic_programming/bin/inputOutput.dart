import 'package:dart_basic_programming/dart_basic_programming.dart';
import 'dart:io';

void main(List<String> args) {
  stdout.write("Masukkan nama depan : ");
  String firstName = stdin.readLineSync()!;
  stdout.write("Masukkan nama belakang : ");
  String lastName = stdin.readLineSync()!;

  print("Nama Anda adalah : $firstName $lastName");
}
