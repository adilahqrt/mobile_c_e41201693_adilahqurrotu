import 'package:dart_basic_programming/dart_basic_programming.dart';
import 'dart:io';

void main(List<String> args) {
  int a = 7;

  for (var x = 1; x <= a; x++) {
    for (var y = 0; y < x; y++) {
      stdout.write(' #');
    }
    print('');
  }
}
