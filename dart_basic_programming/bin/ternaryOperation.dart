import 'package:dart_basic_programming/dart_basic_programming.dart';
import 'dart:io';

void main(List<String> args) {
  print('Apakah Anda akan menginstall aplikasi Dart?');
  stdout.write('Y/T ');
  String answer = stdin.readLineSync()!;

  var result = (answer == 'y')
      ? print('Anda akan menginstall aplikasi Dart')
      : print('Aborted');
}
