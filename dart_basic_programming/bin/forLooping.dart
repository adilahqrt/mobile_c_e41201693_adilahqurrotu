import 'package:dart_basic_programming/dart_basic_programming.dart';

void main(List<String> args) {
  for (int a = 1; a <= 20; a++) {
    if (a % 3 == 0 && a % 2 == 1) {
      print('I Love Coding');
    } else if (a % 2 == 0) {
      print('Berkualitas');
    } else if (a % 2 == 1) {
      print('Santai');
    }
  }
}
