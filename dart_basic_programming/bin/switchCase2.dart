import 'package:dart_basic_programming/dart_basic_programming.dart';

void main(List<String> args) {
  var date = 25;
  var month = 3;
  var year = 2002;

  switch (month) {
    case 1:
      {
        print('$date Januari $year');
        break;
      }
    case 2:
      {
        print('$date Februari $year');
        break;
      }
    case 3:
      {
        print('$date Maret $year');
        break;
      }
    case 4:
      {
        print('$date April $year');
        break;
      }
    case 5:
      {
        print('$date Mei $year');
        break;
      }
    case 6:
      {
        print('$date Juni $year');
        break;
      }
    case 7:
      {
        print('$date Juli $year');
        break;
      }
    case 8:
      {
        print('$date Agustus $year');
        break;
      }
    case 9:
      {
        print('$date September $year');
        break;
      }
    case 10:
      {
        print('$date Oktober $year');
        break;
      }
    case 11:
      {
        print('$date November $year');
        break;
      }
    case 12:
      {
        print('$date Desember $year');
        break;
      }
    default:
      {
        print('Bulan yang Anda masukkan salah!');
      }
  }
}
