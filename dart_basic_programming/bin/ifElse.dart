import 'package:dart_basic_programming/dart_basic_programming.dart';
import 'dart:io';

void main(List<String> args) {
  stdout.write('Nama = ');
  String nama = stdin.readLineSync()!;
  stdout.write('Peran = ');
  String peran = stdin.readLineSync()!;

  if (nama == "") {
    print('Nama harus diisi!');
  } else if (peran == "") {
    print('Halo $nama, Pilih peranmu untuk memulai game!');
  } else if (peran == "penyihir") {
    print(
        'Selamat datang di Dunia Werewolf, $nama \nHalo Penyihir $nama, kamu dapat melihat siapa yang menjadi werewolf!');
  } else if (peran == "guard") {
    print(
        'Selamat datang di Dunia Werewolf, $nama \nHalo Guard $nama, kamu akan membantu melindungi temanmu dari serangan werewolf!');
  } else if (peran == "werewolf") {
    print(
        'Selamat datang di Dunia Werewolf, $nama \nHalo Werewolf $nama, kamu akan memakan mangsa setiap malam!');
  } else {
    print('Error!');
  }
}
