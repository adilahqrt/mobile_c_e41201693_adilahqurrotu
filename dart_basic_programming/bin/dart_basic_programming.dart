import 'package:dart_basic_programming/dart_basic_programming.dart'
    as dart_basic_programming;

void main(List<String> arguments) {
  var world = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';
  print(world +
      " " +
      second +
      " " +
      third +
      " " +
      fourth +
      " " +
      fifth +
      " " +
      sixth +
      " " +
      seventh);
}
