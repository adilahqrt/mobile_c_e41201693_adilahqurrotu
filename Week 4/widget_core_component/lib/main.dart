import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Widget Core Component'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
            child: Column(
              children: const <Widget>[
                Text("Nasi + Mie Rebus"),
                Text("Nasi + Mie Rebus + Telor"),
                Text("Nasi + Ayam"),
                Text("Nasi + Ayam + Es Jeruk"),
                Text("Nasi + Ikan + Teh Manis"),
                Text("Nasi + Sate Ayam"),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
            child: Column(
              children: const <Widget>[
                Text(
                  'Ini Text',
                  style: TextStyle(
                      backgroundColor: Colors.pink,
                      color: Colors.blue,
                      fontSize: 20.0,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Column(
                  children: const <Widget>[Icon(Icons.chat), Text('Chats')],
                ),
                Column(
                  children: const <Widget>[Icon(Icons.history), Text('Status')],
                ),
                Column(
                  children: const <Widget>[Icon(Icons.phone), Text('Calls')],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.all(32.0),
            margin: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0), color: Colors.pink),
            child: const Text(
              'Hello',
              style: TextStyle(color: Colors.white, fontSize: 20.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 10.0, 8.0, 0),
            child: Column(
              children: <Widget>[
                RaisedButton(
                  color: Colors.amber,
                  child: const Text("Raised Button"),
                  onPressed: () {},
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0),
                  child: Column(
                    children: <Widget>[
                      MaterialButton(
                        color: Colors.lime,
                        child: const Text("Material Button"),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0),
                  child: Column(
                    children: <Widget>[
                      FlatButton(
                        color: Colors.lightGreenAccent,
                        child: const Text("FlatButton Button"),
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10.0, 0, 0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(hintText: "Email"),
                  keyboardType: TextInputType.emailAddress,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 5.0, 0, 0),
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        obscureText: true,
                        decoration: const InputDecoration(hintText: "Password"),
                        keyboardType: TextInputType.visiblePassword,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      TextButton(
                        child: const Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        ),
                        style: TextButton.styleFrom(
                          backgroundColor: Colors.blue,
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
